Source: opm-simulators
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Arne Morten Kvarving <arne.morten.kvarving@sintef.no>, Markus Blatt <markus@dr-blatt.de>
Build-Depends: debhelper-compat (= 13), quilt, dh-sequence-python3, bc, chrpath,
               libboost-system-dev, libboost-date-time-dev, libboost-test-dev,
               zlib1g-dev, gfortran, pkgconf, lsb-release, libtool, doxygen,
               texlive-latex-extra, texlive-latex-recommended, ghostscript,
               libopm-grid-dev (>= 2024.10),
	       mpi-default-dev, mpi-default-bin, python3-dev, libpython3-dev,
	       pybind11-dev, libhdf5-mpi-dev
Standards-Version: 4.7.0
Section: libs
Homepage: https://opm-project.org
Vcs-Browser: https://salsa.debian.org/science-team/opm-simulators
Vcs-Git: https://salsa.debian.org/science-team/opm-simulators.git
Rules-Requires-Root: no

Package: libopm-models-dev
Architecture: all
Section: oldlibs
Depends: libopm-simulators-dev (>= 2024.10~),
         ${misc:Depends}
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: libopm-simulators-dev
Section: libdevel
Architecture: amd64 arm64 armel ia64 m68k mips64el mipsel ppc64el riscv64 loong64
Multi-Arch: same
Depends: ${opm:shared-library}, ${misc:Depends}, libopm-grid-dev (>= 2024.10),
         libboost-date-time-dev, python3-opm-common
Replaces: libopm-simulators1-dev,
          libopm-models-dev (<< 2024.10~)
Suggests: libopm-simulators-doc
Breaks: libopm-models-dev (<< 2024.10~)
Description: Parallel porous media / reservoir simulators -- development files
 The Open Porous Media (OPM) software suite provides libraries and
 tools for modeling and simulation of porous media processes, especially
 for simulating CO2 sequestration and improved and enhanced oil recovery.
 .
 opm-simulators provides a research (ebos) and a production (flow) fully
 implicit black-oil simulators, supporting one to three phases and
 supporting solvent and polymer options. It uses cell centered finite
 volume schemes with two point flux approximation and automatic
 differentiation for the discretization and uses state of the art linear
 and nonlinear solvers. It supports standard and multi segment well
 models and reading and writing file in Eclipse format, a very common
 format used in the oil reservoir simulation community.
 .
 Package provides the development files (headers and shared library links).

Package: libopm-simulators
Pre-Depends: ${misc:Pre-Depends}
Architecture: amd64 arm64 armel ia64 m68k mips64el mipsel ppc64el riscv64 loong64
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends}
Provides: ${opm:shared-library}
Replaces: libopm-simulators1
Description: Open porous media / reservoir simulators -- library
 The Open Porous Media (OPM) software suite provides libraries and
 tools for modeling and simulation of porous media processes, especially
 for simulating CO2 sequestration and improved and enhanced oil recovery.
 .
 opm-simulators provides a research (ebos) and a production (flow) fully
 implicit black-oil simulators, supporting one to three phases and
 supporting solvent and polymer options. It uses cell centered finite
 volume schemes with two point flux approximation and automatic
 differentiation for the discretization and uses state of the art linear
 and nonlinear solvers. It supports standard and multi segment well
 models and reading and writing file in Eclipse format, a very common
 format used in the oil reservoir simulation community.
 .
 Package provides the library used by the research (ebos) and production
 (flow) fully implicit  black-oil simulators.

Package: libopm-simulators-bin
Section: science
Pre-Depends: ${misc:Pre-Depends}
Architecture: amd64 arm64 armel ia64 m68k mips64el mipsel ppc64el riscv64 loong64
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}
Replaces: libopm-simulators1-bin
Recommends: libopm-grid-bin, libopm-common-bin
Description: Parallel porous media / reservoir simulators -- applications
 The Open Porous Media (OPM) software suite provides libraries and
 tools for modeling and simulation of porous media processes, especially
 for simulating CO2 sequestration and improved and enhanced oil recovery.
 .
 opm-simulators provides a research (ebos) and a production (flow) fully
 implicit black-oil simulators, supporting one to three phases and
 supporting solvent and polymer options. It uses cell centered finite
 volume schemes with two point flux approximation and automatic
 differentiation for the discretization and uses state of the art linear
 and nonlinear solvers. It supports standard and multi segment well
 models and reading and writing file in Eclipse format, a defacto
 industry standard in the oil reservoir simulation community.
 .
 Package provides the simulation programs.

Package: libopm-models-doc
Architecture: all
Section: oldlibs
Depends: libopm-simulators-doc (>= 2024.10~),
         ${misc:Depends}
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: libopm-simulators-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Provides: libopm-simulators-doc
Breaks: libopm-models-doc (<< 2024.10~)
Replaces: libopm-models-doc (<< 2024.10~)
Description: Open porous media / reservoir simulators -- documentation
 The Open Porous Media (OPM) software suite provides libraries and
 tools for modeling and simulation of porous media processes, especially
 for simulating CO2 sequestration and improved and enhanced oil recovery.
 .
 opm-simulators provides a research (ebos) and a production (flow) fully
 implicit black-oil simulators, supporting one to three phases and
 supporting solvent and polymer options. It uses cell centered finite
 volume schemes with two point flux approximation and automatic
 differentiation for the discretization and uses state of the art linear
 and nonlinear solvers. It supports standard and multi segment well
 models and reading and writing file in Eclipse format, a very common
 format used in the oil reservoir simulation community.
 .
 Package provides documentation of the source code of the simulators.

Package: python3-opm-simulators
Section: python
Pre-Depends: ${misc:Pre-Depends}
Architecture: amd64 arm64 armel ia64 m68k mips64el mipsel ppc64el riscv64 loong64
#Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends}, libopm-simulators
Recommends: python3-opm-common
Description: Python wrappers for the Open porous media / reservoir simulators
 The Open Porous Media (OPM) software suite provides libraries and
 tools for modeling and simulation of porous media processes, especially
 for simulating CO2 sequestration and improved and enhanced oil recovery.
 .
 opm-simulators provides a research (ebos) and a production (flow) fully
 implicit black-oil simulators, supporting one to three phases and
 supporting solvent and polymer options. It uses cell centered finite
 volume schemes with two point flux approximation and automatic
 differentiation for the discretization and uses state of the art linear
 and nonlinear solvers. It supports standard and multi segment well
 models and reading and writing file in Eclipse format, a very common
 format used in the oil reservoir simulation community.
 .
 Package provides Python wrappers for the simulators.
